import React, { Component }   from 'react';
import './App.css';
import Login from '../../helloworld/src/components/Login';
import { Route, Switch } from "react-router-dom";
import Dashboard from '../../helloworld/src/components/Dashboard';

export default class App extends Component {
  state = {
    username: "",
    password: ""

  }

  uchanger = (event) => {
    this.setState({
      username: event.target.value
    })
  }

  pchanger = (event) => {
    this.setState({
      password: event.target.value
    })
  }
  
  welcome = (event) => {
    if (this.state.username === 'josh' && this.state.password === '123') {
      alert('Welcome, ' + this.state.username + '!')
    } 
    else {
      alert('Username or Password is incorrect')
    }
  }
  

  render() {
    return (
      <div>
        <Login uchanger={this.uchanger} pchanger={this.pchanger} click={this.welcome} />

        <Switch>
          <Route exact path="/" component={ Dashboard } />
          <Route path="/login" component={ Login } />
        </Switch>
      </div>
      );
    }
  }

